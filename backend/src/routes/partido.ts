import express from 'express'
import * as partidosController from '../controllers/partido'

const router = express.Router()

// Rutas para los partidos
router.get('/:id', partidosController.obtenerPartidoPorId)
router.put('/:id', partidosController.actualizarPartido)
router.delete('/:id', partidosController.borrarPartido)

export default router
