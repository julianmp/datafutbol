import express from 'express'
import * as equipoController from '../controllers/equipo'

const router = express.Router()

// Rutas para los equipos
router.post('/', equipoController.crearEquipo)
router.get('/', equipoController.obtenerEquipos)
router.get('/:id', equipoController.obtenerEquipoPorId)
router.put('/:id', equipoController.actualizarEquipo)
router.delete('/:id', equipoController.eliminarEquipo)

export default router
