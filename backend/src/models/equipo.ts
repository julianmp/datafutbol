import mongoose from "mongoose";
import { IEquipo } from "../types/equipo";

// Definir el esquema para los equipos
const equipoSchema = new mongoose.Schema<IEquipo>({
  nombre: {
    type: String,
    required: true,
    unique: true,
  },
});

// Crear el modelo de datos para los equipos
export const Equipo = mongoose.model<IEquipo>("Equipo", equipoSchema);
