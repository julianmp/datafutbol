import mongoose from 'mongoose'
import { ICompetencia } from '../types/competencia'

// Definir el esquema para las competencias
const competenciaSchema = new mongoose.Schema<ICompetencia>(
	{
		nombre: {
			type: String,
			required: true,
		},
		equipos: [
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Equipo',
			},
		],
		partidos: [
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Partido',
			},
		],
	},
	{
		timestamps: true,
	}
)

// Crear el modelo de datos para las competencias
export const Competencia = mongoose.model<ICompetencia>('Competencia', competenciaSchema)
