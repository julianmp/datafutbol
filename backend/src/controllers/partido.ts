import { Request, Response } from "express";
import { IPartido } from "../types/partido";
import { Partido } from "../models/partido";

export const obtenerPartidoPorId = async (
  req: Request,
  res: Response
): Promise<void> => {
  try {
    const partido: IPartido | null = await Partido.findById(req.params.id);
    res.status(200).json(partido);
  } catch (error: any) {
    console.error(error);
    res.status(500).send(error.message);
  }
};

export const actualizarPartido = async (
  req: Request,
  res: Response
): Promise<void> => {
  try {
    const partidoActualizado: IPartido | null = await Partido.findByIdAndUpdate(
      req.params.id,
      req.body,
      { new: true }
    );
    res.status(200).json(partidoActualizado);
  } catch (error: any) {
    console.error(error);
    res.status(500).send(error.message);
  }
};

export const borrarPartido = async (
  req: Request,
  res: Response
): Promise<void> => {
  try {
    const partidoEliminado: IPartido | null = await Partido.findByIdAndDelete(
      req.params.id
    );
    res.status(200).json(partidoEliminado);
  } catch (error: any) {
    console.error(error);
    res.status(500).send(error.message);
  }
};
