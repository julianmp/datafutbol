import express from 'express'
import cors from 'cors'
import mongoose from 'mongoose'
import morgan from 'morgan'

import equiposRouter from './routes/equipo'
import competenciasRouter from './routes/compentencia'
import partidosRouter from './routes/partido'
//import { errorHandler } from "./middleware/error-handler";
//import { NotFoundError } from "./errors/not-found-error";

export const app = express()
const port = 3000

app.use(express.json())
app.use(cors())
app.use(morgan('combined'))

// Rutas de la API
app.use('/api/v1/equipos', equiposRouter)
app.use('/api/v1/competencias', competenciasRouter)
app.use('/api/v1/partidos', partidosRouter)

// Ruta de prueba
app.get('/', (req, res) => res.send({}))

// Middleware para manejar errores de rutas no encontradas
//app.all("*", (req, res, next) => {
//  next(new NotFoundError());
//});

// Middleware para manejar errores de la aplicación
//app.use(errorHandler);

mongoose.set('strictQuery', false)
mongoose.connect('mongodb://localhost:27017/datafutbol', (err) => {
	if (err) return console.error(err)
	app.listen(port, () => {
		console.log(`Servidor iniciado en http://localhost:${port}`)
	})
})
