import { Document } from "mongoose";

export interface IEquipo extends Document {
  nombre: string;
}
