import { Document } from "mongoose";
import { ICompetencia } from "./competencia";
import { IEquipo } from "./equipo";

export interface IPartido extends Document {
  competencia: ICompetencia["_id"];
  fecha: number;
  equipoLocal: IEquipo["_id"];
  equipoVisitante: IEquipo["_id"];
  resultado: {
    golesLocal: number;
    golesVisitante: number;
  };
}
