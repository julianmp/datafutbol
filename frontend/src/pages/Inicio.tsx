import React from 'react'
import { Header, Icon, Image } from 'semantic-ui-react'

const Inicio: React.FC = () => {
	return (
		<div style={{ margin: 'auto' }}>
			<Header as="h2" icon textAlign="center">
				<Icon name="settings" circular />
				<Header.Content>Sitio en construccion</Header.Content>
				<Header.Subheader>
					Visite <a href="/competencias">Competencias</a>
				</Header.Subheader>
			</Header>
		</div>
	)
}

export default Inicio
