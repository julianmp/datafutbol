import React from 'react'
import { Route, Routes, useNavigate, useParams } from 'react-router-dom'
import { useCompetenciaById } from '../hooks/useCompetenciaById'
import { Header, Breadcrumb, Container, Segment, Icon, Loader } from 'semantic-ui-react'
import { ListaEquipos } from '../components/ListaEquipos'
import { TablaPosiciones } from '../components/TablaPosiciones'
import { Fixture } from '../components/Fixture'
import { ChipNavigator, IChipElement } from '../components/ChipsNavigator'

const CompetenciaDetalle: React.FC = () => {
	const { id } = useParams()
	const navigate = useNavigate()
	const { competencia, agregarEquipo, generarFixture } = useCompetenciaById(id!)

	if (!competencia) return <Loader active inline="centered" content="Loading" />

	const equipos = competencia.equipos
	const partidos = competencia.partidos

	const routes: IChipElement[] = [
		{ icon: 'ordered list', text: 'Posiciones', url: './' },
		{ icon: 'shield', text: 'Equipos', url: './equipos' },
		{ icon: 'calendar alternate', text: 'Fixture', url: './fixture' },
		{ icon: 'star', text: 'Estadisticas', url: './estadisticas' },
	]

	return (
		<>
			<Header as="h3" block>
				<Breadcrumb>
					<Breadcrumb.Section href="/competencias">Competencias</Breadcrumb.Section>
					<Breadcrumb.Divider />
					<Breadcrumb.Section link>{competencia.nombre}</Breadcrumb.Section>
				</Breadcrumb>
			</Header>
			<Container text>
				<ChipNavigator routes={routes} />
				<Routes>
					<Route path="/" element={<TablaPosiciones idCompetencia={id!} />} />
					<Route path="/equipos" element={<ListaEquipos equipos={equipos} onSelect={(e) => navigate(`/equipos/${e._id}`)} />} />
					<Route path="/fixture" element={<Fixture partidos={partidos} equipos={equipos} />} />
					<Route
						path="/estadisticas"
						element={
							<Segment placeholder>
								<Header icon>
									<Icon name="settings" />
									Aun no hay estadisticas.
								</Header>
							</Segment>
						}
					></Route>
				</Routes>
			</Container>
		</>
	)
}

export default CompetenciaDetalle
