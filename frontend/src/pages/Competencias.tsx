import React, { useRef } from 'react'
import { useNavigate } from 'react-router-dom'
import { List, Image, Input, Button, Header } from 'semantic-ui-react'
import { useCompetencias } from '../hooks/useCompetencias'

const Competencias: React.FC = () => {
	const navigate = useNavigate()
	const { competencias, createCompetencia } = useCompetencias()

	const inputRef = useRef<HTMLInputElement>(null)

	const handleClick = async () => {
		const nombre = inputRef.current?.value
		if (!nombre) return
		createCompetencia(nombre)
		inputRef.current.value = ''
	}

	return (
		<div>
			<Header as="h3" block>
				Competencias
			</Header>
			{competencias.length > 0 ? (
				<List divided relaxed selection>
					<Input placeholder="Crear competencia" action fluid>
						<input ref={inputRef} />
						<Button onClick={handleClick}>Crear</Button>
					</Input>
					{competencias.map((competencia) => (
						<List.Item key={competencia._id} onClick={() => navigate(`/competencias/${competencia._id}`)}>
							<Image avatar src="/public/trofeo.png" />
							<List.Content>
								<List.Header as="a">{competencia.nombre}</List.Header>
								<List.Description as="a">Updated 10 mins ago</List.Description>
							</List.Content>
						</List.Item>
					))}
				</List>
			) : (
				<p>No hay competencias registradas</p>
			)}
		</div>
	)
}

export default Competencias
