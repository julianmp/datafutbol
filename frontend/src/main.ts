import './styles/main.css'
import 'semantic-ui-css/semantic.min.css'

import * as React from 'react'
import * as ReactDOM from 'react-dom/client'
import MainRouter from './routes/index'

const root = ReactDOM.createRoot(document.getElementById('root')!)
const component = React.createElement(MainRouter)

root.render(component)
