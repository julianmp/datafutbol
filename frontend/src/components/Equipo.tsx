import React from 'react'
import { Header, Image } from 'semantic-ui-react'
import { IEquipo } from '../types/equipos'

export const Equipo: React.FC<IEquipo & { espejado?: boolean; as?: any }> = ({ nombre, espejado, as = 'h4' }) => {
	const imagen = '/public/escudo.png'

	return espejado ? (
		<Header as={as}>
			{nombre} <Image spaced src={imagen} />
		</Header>
	) : (
		<Header as={as}>
			<Image spaced src={imagen} /> {nombre}
		</Header>
	)
}
