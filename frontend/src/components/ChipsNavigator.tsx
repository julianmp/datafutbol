import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Icon, Label, SemanticICONS } from 'semantic-ui-react'

export interface IChipElement {
	icon: SemanticICONS
	text: string
	url: string
}

export const ChipNavigator: React.FC<{ routes: IChipElement[] }> = ({ routes }) => {
	const navigate = useNavigate()

	return (
		<Label.Group size="large">
			{routes.map(({ icon, text, url }, idx) => (
				<Label key={idx} as="a" basic onClick={() => navigate(url)}>
					<Icon name={icon} /> {text}
				</Label>
			))}
		</Label.Group>
	)
}
