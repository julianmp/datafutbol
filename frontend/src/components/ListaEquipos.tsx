import React from 'react'
import { List, Segment } from 'semantic-ui-react'
import { IEquipo } from '../types/equipos'
import { BuscarEquipo } from './BuscarEquipo'
import { Equipo } from './Equipo'

type ListaEquiposProps = {
	equipos: IEquipo[]
	onSelect: (equipo: IEquipo) => void
}

export const ListaEquipos: React.FC<ListaEquiposProps> = ({ equipos, onSelect }) => {
	return (
		<Segment>
			<BuscarEquipo onSelect={(e) => {}} />
			<List animated divided relaxed selection>
				{equipos.map((e) => (
					<List.Item key={e._id} onClick={() => onSelect(e)}>
						<List.Content>
							<Equipo {...e} />
						</List.Content>
					</List.Item>
				))}
			</List>
		</Segment>
	)
}
