import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Grid } from 'semantic-ui-react'
import { IEquipo } from '../types/equipos'
import { IPartido } from '../types/partido'
import { Equipo } from './Equipo'

type PartidoProps = { _id: string; equipoLocal: IEquipo; equipoVisitante: IEquipo; resultado?: IPartido['resultado'] }

export const Partido: React.FC<PartidoProps> = ({ _id, equipoLocal, equipoVisitante, resultado }) => {
	const navigate = useNavigate()

	return (
		<Grid.Row key={_id} onClick={() => navigate(`/partidos/${_id}`)}>
			<Grid.Column textAlign="left">
				<Equipo {...equipoLocal!} />
			</Grid.Column>
			<Grid.Column verticalAlign="middle">{resultado ? `${resultado.golesLocal} - ${resultado.golesVisitante}` : 'vs'}</Grid.Column>
			<Grid.Column textAlign="right">
				<Equipo espejado {...equipoVisitante!} />
			</Grid.Column>
		</Grid.Row>
	)
}
