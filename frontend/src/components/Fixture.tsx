import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Button, Grid, Header, Icon, Segment } from 'semantic-ui-react'
import { IEquipo } from '../types/equipos'
import { IPartido } from '../types/partido'
import { Partido } from './Partido'

export const Fixture: React.FC<{ partidos: IPartido[]; equipos: IEquipo[] }> = ({ partidos, equipos }) => {
	const getEquipo = (id: string) => equipos.find((e) => e._id === id)

	return (
		<Segment>
			<Grid columns={3} textAlign="center">
				{partidos.length !== 0 ? (
					partidos.map((partido) => {
						const equipoLocal = getEquipo(partido.equipoLocal)!
						const equipoVisitante = getEquipo(partido.equipoVisitante)!

						return <Partido key={partido._id} {...partido} equipoLocal={equipoLocal} equipoVisitante={equipoVisitante} />
					})
				) : (
					<Segment placeholder>
						<Header icon>
							<Icon name="settings" />
							Aun no hay partidos.
						</Header>
						<Segment.Inline>
							<Button primary disabled={equipos.length < 3} onClick={() => {}}>
								Generar fixture
							</Button>
							<Button color="red">Eliminar competencia</Button>
						</Segment.Inline>
					</Segment>
				)}
			</Grid>
		</Segment>
	)
}
