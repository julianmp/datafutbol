import { ICompetencia } from './competencia'
import { IEquipo } from './equipos'

export interface IPartido {
	_id: string
	name: string
	competencia: ICompetencia['_id']
	equipoLocal: IEquipo['_id']
	equipoVisitante: IEquipo['_id']
	fecha: number
	resultado: { golesLocal: number; golesVisitante: number }
	updatedAt: Date
	createdAt: Date
}
