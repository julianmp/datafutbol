import { IEquipo } from './equipos'
import { IPartido } from './partido'

export interface ICompetencia {
	_id: string
	nombre: string
	createdAt: Date
	updatedAt: Date
	equipos: IEquipo[]
	partidos: IPartido[]
}
