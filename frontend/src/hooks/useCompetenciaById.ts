import axios from 'axios'
import { useEffect, useState } from 'react'
import { ICompetencia } from '../types/competencia'
import { IEquipo } from '../types/equipos'

export const useCompetenciaById = (id: string) => {
	const [competencia, setCompetencia] = useState<ICompetencia>()

	useEffect(() => {
		axios(`http://localhost:3000/api/v1/competencias/${id}`).then((resp) => setCompetencia(resp.data))
	}, [id])

	const agregarEquipo = async (equipo: IEquipo) => {
		await axios.post(`http://localhost:3000/api/v1/competencias/${id}/equipos`, { equipoId: equipo._id })
		setCompetencia((c) => ({ ...c!, equipos: [...c!.equipos, equipo] }))
	}

	const generarFixture = async () => {
		if (competencia?.equipos.length! < 3) return
		const response = await axios.post(`http://localhost:3000/api/v1/competencias/${id}/partidos`)
		setCompetencia((c) => ({ ...c!, partidos: response.data }))
	}

	return { competencia, agregarEquipo, generarFixture }
}
