import React from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Inicio from '../pages/Inicio'
import Competencias from '../pages/Competencias'
import CompetenciaDetalle from '../pages/CompetenciaDetalle'
import EquipoDetalle from '../pages/EquipoDetalle'
import AdminCompetencias from '../pages/AdminCompetencias'
import AdminCompetenciaDetalle from '../pages/AdminCompetenciaDetalle'
import AdminEquipoDetalle from '../pages/AdminEquipoDetalle'
import NotFound from '../pages/NotFound'

const AppRouter: React.FC = () => {
	return (
		<Router>
			<Routes>
				<Route path="/" element={<Inicio />} />
				<Route path="/competencias" element={<Competencias />} />
				<Route path="/competencias/:id/*" element={<CompetenciaDetalle />} />
				<Route path="/equipos/:id" element={<EquipoDetalle />} />
				<Route path="/admin/competencias" element={<AdminCompetencias />} />
				<Route path="/admin/competencias/:id" element={<AdminCompetenciaDetalle />} />
				<Route path="/admin/equipos/:id" element={<AdminEquipoDetalle />} />
				<Route path="*" element={<NotFound />} />
			</Routes>
		</Router>
	)
}

export default AppRouter
